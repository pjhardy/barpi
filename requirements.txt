Cython==0.27.1
gpiozero==1.4.0
numpy==1.13.3
paho-mqtt==1.3.1
pkg-resources==0.0.0
pyalsaaudio==0.8.4
rpi-ws281x==3.0.3
RPi.GPIO==0.6.3

