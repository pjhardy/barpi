#!/usr/bin/env python3
from gpiozero import Button
import json
from neopixel import *
import paho.mqtt.client as mqtt
from signal import pause
from threading import Lock, Thread

import configuration
import rainbow
import throb
import utils

class LightStrip:
    LED_COUNT = 60
    LED_PIN = 18
    LED_FREQ_HZ = 800000
    LED_DMA = 5
    LED_INVERT = False
    LED_CHANNEL = 0
    #LED_STRIP = ws.WS2811_STRIP_GRB

    def __init__(self):
        self.lightsOn = False
        self.defaultOnColor = Color(255, 255, 255)
        self.singleColor = Color(0, 0, 0)
        self.brightness = 255
        print("Initialising strip")
        self.strip = Adafruit_NeoPixel(self.LED_COUNT, self.LED_PIN,
                                       self.LED_FREQ_HZ, self.LED_DMA,
                                       self.LED_INVERT, self.brightness,
                                       self.LED_CHANNEL)
        self.strip.begin()

    def showSingleColor(self, color):
        self.singleColor = color
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, color)
        self.strip.show()

    def setBrightness(self, brightness):
        self.strip.setBrightness(brightness)
        self.strip.show()

    def turnOn(self):
        self.showSingleColor(self.defaultOnColor)
        self.lightsOn = True

    def turnOff(self):
        print("Turning lights off")
        self.showSingleColor(Color(0, 0, 0))
        self.lightsOn = False

    def toggleLights(self):
        if self.lightsOn:
            self.turnOff()
        else:
            self.turnOn(Color(255, 255, 255))

class Barpi:
    def __init__(self, config, lightstrip, sound_activated_lock, rainbow_lock):
        self.config = config
        self.lightstrip = lightstrip
        self.lightstrip.turnOff()
        self.light_lock = Lock()
        self.sound_activated_lock = sound_activated_lock
        self.sound_activated_lock.acquire()
        self.rainbow_lock = rainbow_lock
        self.rainbow_lock.acquire()

        # default color is a nice pale blue.
        self.default_color = {
            "r": 157,
            "g": 170,
            "b": 255
        }
        # default color_temp is something that seem to work for us.
        self.default_color_temp = 367

        self.lightStatus = {
            "state": "OFF",
            "brightness": 255,
            "color_temp": self.default_color_temp,
            "color": self.default_color,
            "effect": "Single colour",
        }

        self.mqtt = self.setup_mqtt(config['mqtt'])
        self.publish_status()
        self.powerButton = Button(5)
        self.modeButton = Button(6)
        self.powerButton.when_pressed = self.power_button_handler
        self.modeButton.when_pressed = self.mode_button_handler

    def power_button_handler(self):
        print("power button pressed")

        if self.lightStatus['state'] == 'OFF':
            default_color = Color(self.default_color['r'],
                                  self.default_color['g'],
                                  self.default_color['b'])
            with self.light_lock:
                self.lightstrip.setBrightness(255)
                self.lightstrip.showSingleColor(default_color)
            self.lightStatus['state'] = 'ON'
            self.lightStatus['brightness'] = 255
            self.lightStatus['color'] = self.default_color
        else:
            with self.light_lock:
                self.lightstrip.turnOff()
            self.lightStatus['state'] = 'OFF'

        self.publish_status()

    def mode_button_handler(self):
        print("mode button pressed")
        if self.lightStatus['effect'] == 'Sound throb':
            self.effect_handler('Single colour')
        elif self.lightStatus['effect'] == 'Single colour':
            self.effect_handler('Sound throb')

        self.publish_status()

    def run(self):
        self.mqtt.loop_start()

    def setup_mqtt(self, config):
        def on_connect(client, userdata, flags, rc):
            print("MQTT connected with result code {}".format(str(rc)))
            client.subscribe(config["CommandTopic"])

        client = mqtt.Client(client_id="barpi")
        client.username_pw_set(username=config["Username"],
                               password=config["Password"])
        client.on_connect = on_connect
        client.on_message = self.mqtt_handler
        client.connect(config["Server"], 1883, 60)

        return client

    def mqtt_handler(self, client, userdata, msg):
        print("mqtt received {}: {}".format(msg.topic, msg.payload.decode()))
        if (msg.topic == 'edgar/barpi/command'):
            payload = json.loads(msg.payload.decode())
            for key in payload.keys():
                if key == 'state':
                    self.state_handler(payload['state'])
                elif key == 'brightness':
                    self.brightness_handler(payload['brightness'])
                elif key == 'color':
                    self.color_handler(payload['color'])
                elif key == 'color_temp':
                    self.color_temp_handler(payload['color_temp'])
                elif key == 'effect':
                    self.effect_handler(payload['effect'])
        else:
            print("Got unknown topic")
        self.publish_status()

    def publish_status(self):
        self.mqtt.publish(self.config['mqtt']['StatusTopic'],
                       json.dumps(self.lightStatus))

    def state_handler(self, state):
        # hass sends "state": "ON" for all light operations,
        # so we have to make sure we only set state if it's
        # changed.
        if state == self.lightStatus['state']:
            return

        if state == 'ON':
            # This should restore the last color we had set
            self.color_handler(self.lightStatus['color'])
            self.lightStatus['state'] = 'ON'
        else:
            with self.light_lock:
                self.lightstrip.turnOff()
            self.lightStatus['state'] = 'OFF'

    def brightness_handler(self, brightness):
        if brightness > 128:
            brightness = 64
        else:
            brightness = 0
        with self.light_lock:
            self.lightstrip.setBrightness(brightness)
        self.lightStatus['brightness'] = brightness

    def color_handler(self, color):
        red = color['r']
        green = color['g']
        blue = color['b']
        with self.light_lock:
            self.lightstrip.showSingleColor(Color(red, green, blue))
        self.lightStatus['color'] = color

    def color_temp_handler(self, color_temp):
        new_color = utils.mired_to_color(color_temp)
        with self.light_lock:
            self.lightstrip.showSingleColor(new_color)
        self.lightStatus['color_temp'] = color_temp

    def effect_handler(self, effect):
        # TODO: Locks won't scale. Find a better way to
        # manage this if we're going to add more effects.
        if effect == "Rainbow":
            if self.lightStatus['effect'] == "Sound throb":
                self.sound_activated_lock.acquire()
            with self.light_lock:
                self.lightstrip.setBrightness(255)
            self.rainbow_lock.release()
        if effect == "Sound throb":
            if self.lightStatus['effect'] == "Rainbow":
                self.rainbow_lock.acquire()
            with self.light_lock:
                self.lightstrip.setBrightness(0)
            self.sound_activated_lock.release()
        elif effect == "Single colour":
            if self.lightStatus['effect'] == "Sound throb":
                self.sound_activated_lock.acquire()
            elif self.lightStatus['effect'] == "Rainbow":
                self.rainbow_lock.acquire()
            with self.light_lock:
                self.lightstrip.setBrightness(self.lightStatus['brightness'])
        else:
            return
        self.lightStatus['effect'] = effect

def set_brightness(brightness):
    for value in brightness:
        scaled = int(brightness * 255)
        #print("Setting brightness to {}".format(scaled))
        barpi.brightness_handler(scaled)

if __name__ == '__main__':
    sound_activated_lock = Lock()
    rainbow_lock = Lock()
    config = configuration.Configuration()
    lightstrip = LightStrip()
    barpi = Barpi(config, lightstrip, sound_activated_lock, rainbow_lock)
    barpi.run()

    throb_thread = Thread(target = throb.audio_in, name = "throb_thread",
                          args = (config['barpi'], sound_activated_lock,
                                  set_brightness))
    throb_thread.start()
    # I just realised this is pretty gross. 
    rainbow.rainbow_cycle(rainbow_lock, barpi.lightstrip.strip)
