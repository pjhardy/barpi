# This class adapts lightshowpi's audio-in mode for barpi.
# lightshowpi is licensed under the BSD license. Refer to
# LICENSE in the root direcory for full license.
#
# original authors:
# Todd Giles (todd@lightshowpi.com)
# Chris Usey (chris.usey@gmail.com)
# Ryan Jennings
# Paul Dunn (dunnsept@gmail.com)
# Tom Enos (tomslick.ca@gmail.com)

import alsaaudio as aa
import audioop
from collections import deque
import numpy as np
from threading import Thread
import time

import fft
import RunningStats

streaming = None

def audio_in(config, sound_activated_lock, brightness_handler):
    global streaming
    stream_reader = None
    streaming = None

    sample_rate = int(config['SampleRate'])
    audio_channels = int(config['AudioChannels'])
    chunk_size = int(config['ChunkSize'])

    #streaming = aa.PCM(aa.PCM_CAPTURE, aa.PCM_NORMAL, config['InputDevice'])
    streaming = aa.PCM(type=aa.PCM_CAPTURE, 
            mode=aa.PCM_NORMAL, cardindex=1)
    streaming.setchannels(audio_channels)
    streaming.setformat(aa.PCM_FORMAT_S16_LE)
    streaming.setrate(sample_rate)
    streaming.setperiodsize(chunk_size)

    stream_reader = lambda: streaming.read()[-1]

    # setup light delay
    chunks_per_sec = ((16 * audio_channels * sample_rate) / 8) / chunk_size
    # light_delay should be how long the lights lag behind the audio.
    # Hardcoded to 0 on the assumption we only get audio in from a
    # sound device
    # light_delay = int(config['LightDelay'] * chunks_per_sec)
    light_delay = 0
    matrix_buffer = deque([], 1000)

    # Start with these as our initial guesses - will calculate a rolling mean / std
    # as we get input data.
    light_channels = int(config['LightChannels'])
    mean = np.array([12.0 for _ in range(light_channels)],
                    dtype='float32')
    std = np.array([12.0 for _ in range(light_channels)],
                   dtype='float32')
    count = 2
    running_stats = RunningStats.Stats(light_channels)

    # preload running_stats to avoid errors, and give us a show that looks
    # good right from the start
    running_stats.preload(mean, std, count)

    # Assume the light strip has already been initialised.
    fft_calc = fft.FFT(chunk_size,
                       sample_rate,
                       int(config['LightChannels']),
                       int(config['MinFrequency']),
                       int(config['MaxFrequency']),
                       [], # custom_channel_mapping. Not needed yet
                       [], # custom_channel_frequencies. Not needed yet
                       audio_channels)

    sd_low = float(config['SDLow'])
    sd_high = float(config['SDHigh'])
    # Here is where we loop and listen
    while True:
        with sound_activated_lock:
            try:
                data = stream_reader()
            except OSError as err:
                if err.errno == errno.EAGAIN or err.errno == errno.EWOULDBLOCK:
                    continue
            if len(data):
                # if the maximum of the absolute value of all samples in
                # the data is below a threshold we will disregard it
                audio_max = audioop.max(data, 2)
                if audio_max < 250:
                    # we will fill the matrix with zeros and turn the lights off
                    matrix = np.zeros(light_channels, dtype="float32")
                    print("below threshold: {}, turning the lights off")
                else:
                    matrix = fft_calc.calculate_levels(data)
                    running_stats.push(matrix)
                    mean = running_stats.mean()
                    std = running_stats.std()

                matrix_buffer.appendleft(matrix)

                if len(matrix_buffer) > light_delay:
                    matrix = matrix_buffer[light_delay]
                    update_lights(brightness_handler, sd_low, sd_high,
                                  matrix, mean, std)

def update_lights(brightness_handler, sd_low, sd_high, matrix, mean, std):
    """Update the state of the lights

    Update the state of all the lights based upon the current
    frequency response matrix

    :param brightness_handler: The callback function to pass brightnesses
    to the light strip.
    :type brightness_handler: function

    :param sd_low: The minimum standard deviation, below which lights are off.
    :type sd_low: float

    :param sd_high: The maximum standard deviation, above which lights are max.
    :type sd_high: float

    :param matrix: row of data from cache matrix
    :type matrix: list

    :param mean: standard mean of fft values
    :type mean: list

    :param std: standard deviation of fft values
    :type std: list
    """
    global decay

    # Hard coding attenuate_pct for now.
    # Lower the response value for the lights by a percentage
    # Typical values should be in the range of 20-50
    # Higher values will cause the lights to be more off than on in onoff mode
    # Set to 0 to disable
    attenuate_pct = 0

    brightness = matrix - mean + (std * sd_low)
    brightness = (brightness / (std * (sd_low + sd_high))) * (1.0 - (attenuate_pct / 100.0))
    # ensure that the brightness levels are in the correct range
    brightness = np.clip(brightness, 0.0, 1.0)
    brightness = np.round(brightness, decimals=3)

    # calculate light decay rate if used
    # TODO: bring this over from lightshowpi if needed

    brightness_handler(brightness)
