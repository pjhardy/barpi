#!/usr/bin/env python3

from configparser import ConfigParser

class Configuration(ConfigParser):
    def __init__(self):
        self.home_dir = "."
        self.config_dir = "{}/config".format(self.home_dir)
        self.config_file = "{}/barpi.cfg".format(self.config_dir)
        super().__init__()
        config_handle = open(self.config_file)
        self.read_file(config_handle)
        config_handle.close()
