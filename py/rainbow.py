# Rainbow routines modified from rpi_ws281x strandtest example script
import time

from neopixel import Color

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow_cycle(rainbow_lock, strip, wait_ms=20):
    """Draw rainbow uniformly distributed across all pixels."""
    while True:
        for j in range(256):
            with rainbow_lock:
                for i in range(strip.numPixels()):
                    strip.setPixelColor(i, wheel((int(i*256/strip.numPixels())+j)&255))
                strip.show()
                time.sleep(wait_ms/1000.0)

