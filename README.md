# barpi

A Python3 project to control WS2812B lights connected to a Raspberry Pi.

As well as a basic single colour illumination mode, it has a 
sound-responsive mode, based on the [lightshowpi](http://lightshowpi.org/)
project.

The system provides two buttons, to turn the lights on and off, and switch
between single-colour and sound-responsive mode. It also connects to an
[MQTT](http://mqtt.org/) bus, allowing full control via home automation
systems such as [Home Assistant](https://home-assistant.io/)

## Hardware

* WS2812B addressable LED strip, with the data line connected to GPIO 18
  (pin 12 on the Pi). I've only tested with 60 lights, but it'll
  probably work to the theoretical maximum supported by
  [rpi_ws281x](https://github.com/jgarff/rpi_ws281x).
* Two momentary push buttons: Power connected to GPIO 5, Mode connected
  to GPIO 6.

## Software

* Python 3. The default version in Raspbian Stretch (3.5.3) is fine.
* An MQTT broker. I'm using [https://mosquitto.org/](Mosquitto). The
  built-in broker supplied with Home Assistant also runs fine for this.
  Refer to the setup docs for each for usage.
* [Home Assistant](https://home-assistant.io/). This is optional, but
  barpi sends and receives JSON blobs over MQTT that are easily
  integrated with hass.

## Installation

* Check out the code and change in to the directory.
* Install dependencies

        $ sudo apt-get install libasound2-dev virtualenv

* Create and activate a python virtual environment

        $ virtualenv -p python3 venv
        $ . venv/bin/activate

* Install dependencies. This will take a long time, as cython is
  huge.

        $ pip3 install -r requirements.txt && pip3 install -r requirements2.txt

* Create a configuration file. Refer to comments in the config file for
  how to set it up.

        $ cp config/barpi.cfg.sample config/barpi.cfg
        $ nano config/barpi.cfg

* Run it.

        $ sudo python3 py/barpi.py

## MQTT packet format

barpi subscribes to an MQTT bus. Full control of the colour, brightness
and lighting mode (currently 'Single colour' or 'Sound throb') can be
done by publishing JSON struct. It also publishes a similar JSON struct
whenever it changes state. Note that all of these elements are
optional:

    {
      "brightness": 255,
      "color_temp": 155,
      "color": {
        "r": 255,
        "g": 255,
        "b": 255
      },
      "effect": "Single colour",
      "state": "ON"
    }

## Integrating with Home Assistant

The JSON format barpi uses is compatible with the `mqtt_json` light
platform of [Home Assistant](https://home-assistant.io/). It can be used with a configuration
like this:

    light:
      - platform: mqtt_json
        name: Bar lights
        command_topic: "edgar/barpi/command"
        state_topic: "edgar/barpi/status"
        brightness: true
        color_temp: true
        rgb: true
        effect_list:
          - Single colour
          - Sound throb
          
